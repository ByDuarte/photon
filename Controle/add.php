<?php
require_once("UsrControl.php");
try{
    $usuario = new Usuario();
    $usuario->setNome($_POST["nome"]);
    $usuario->setUser($_POST["usr"]);
    $usuario->setEmail($_POST["email"]);
    $usuario->setSenha($_POST["pwd"]);
    $usuario->setConfSenha($_POST["ConPwd"]);
    $control = new ControleUsuario();
    if ($control->confirmSenha($usuario->getSenha(),$usuario->getConfSenha())) {
        if($control->inserir($usuario)){
            header("Location: ../Visual/Cadastro.php");
        }
    }else{
        echo "Senhas Não coincidem";
    }
}catch(Exception $e){
    echo "Erro: {$e->getMessage()}";
}
?>
