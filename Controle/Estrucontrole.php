<?php
require_once("Conect.php");
require_once("../Modelo/PostMod.php");
    class Postagem{
    	function selecionarPost(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT titulo,descricao FROM posts;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "AddPost");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function EditarPost($id){
        try{
            $conexao = new Conexao();
            $cmd = $conexao->getConexao()->prepare("SELECT * FROM posts WHERE id=:id;");
            $cmd->bindParam("id", $id);
            if($cmd->execute()){
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "Posts");
                return $resultado;
            }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function inserirDados($ha,$img){
            try{
                $conexao = new Conexao();
                $bin = file_get_contents($img['tmp_name']);
                $title = $ha->getTitulo();
                $descri = $ha->getDesc();
                $cmd = $conexao->getConexao()->prepare("INSERT INTO posts(titulo,descricao,nome,tipo,tmp) VALUES(:t,:d,:n,:ti,:tm);");
                $cmd->bindParam("t",$title);
                $cmd->bindParam("d", $descri);
                $cmd->bindParam("n", $img['name']);
                $cmd->bindParam("ti", $img['type']);
                $cmd->bindParam("tm", $bin);
                if($cmd->execute()){
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
                
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function DeletaPost($title){
            try{
                $conexao = new Conexao();
                $del = $conexao->getConexao()->prepare("DELETE  FROM posts WHERE titulo=:title;");
                $del->bindParam("title",$title);
                $del->execute();
                if($del->execute()){
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function UpdatePost($nome,$title){
            try{    
                $conexao = new Conexao();
                $up = $conexao->getConexao()->prepare("UPDATE posts SET titulo=:t,descricao=:d WHERE titulo=:title;");
                $up->bindParam("t",$nome->getTitulo());
                $up->bindParam("d",$nome->getDesc());
                $up->bindParam("title",$title);
                if($up->execute()){
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage}";
            }    
        }
        // function inserirImg($img){
        // try{
        //     $conexao = new Conexao();
        //     $bin = file_get_contents($img['tmp_name']);
        //     $cmd = $conexao->getConexao()->prepare("INSERT INTO imagens(nome,tipo,tmp) VALUES(:n,:ti,:d);");
        //     $cmd->bindParam("n", $img['name']);
        //     $cmd->bindParam("ti", $img['type']);
        //     $cmd->bindParam("d", $bin);
        //         if($cmd->execute()){
        //             return true;
        //         }else{
        //             $conexao->fecharConexao();
        //             return false;
        //         }
        //     }catch(PDOException $e){
        //         echo "Erro do banco: {$e->getMessage()}";
        //         return false;
        //     }catch(Exception $e){
        //         echo "Erro geral: {$e->getMessage()}";
        //         return false;
        //     }
        // }

}
?>