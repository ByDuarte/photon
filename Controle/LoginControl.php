<?php
require_once("Conect.php");
require_once("../Modelo/UsrMod.php");
class ControleLogin{
	function Logar($user){
		$con = new Conexao();
		$sql = "SELECT * FROM usuario WHERE user=:busca;";
		$comando = $con->getConexao()->prepare($sql);
		$comando->bindParam("busca",$user);
		if($comando->execute()){
			$result = $comando->fetchAll(PDO::FETCH_CLASS,"Usuario");
			$con->fecharConexao();
			return $result;
		}else{
			return NULL;
		}
	}
}
?>