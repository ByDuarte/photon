<?php
require_once("../Modelo/UsrMod.php");
require_once("Conect.php");    // Requisição da classe de conexao
    class ControleUsuario{                  // Classe de controle do usuario
        // Preparação dos comandos
        // Pra começo de conversar, todos os métodos terá tratamento de exceção
        function selecionarTodos(){              // Comando SELECT para todos
            try{
                $con=new Conexao();
                $rs = $con->getConexao()->query("SELECT * FROM photon;")->fetchAll();
                if($rs != NULL){
                    echo "<table border='1'>";
                    echo "<tr>";
                    echo "<th>ID</th>";
                    echo "<th>Nome</th>";
                    echo "<th>Usuário</th>";
                    echo "<th>E-Mail</th>";
                    echo "<th>Senha</th>";
                    echo "</tr>";
                    foreach($rs as $item){
                        echo "<tr>";
                        echo "<td>{$item['id']}</td>";
                        echo "<td>{$item['nome']}</td>";
                        echo "<td>{$item['usr']}</td>";
                        echo "<td>{$item['email']}</td>";
                        echo "<td>{$item['senha']}</td>";
                        echo "</tr>";
                    }
                    echo "</table>";
                    }
                $con->fecharConexao();
            }catch(PDOException $e){
                echo "Erro PDO: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function selecionarUm($busca){                // Comando SELECT para um
            try{
                $con=new Conexao();
                $rs=$con->getConexao()->prepare("SELECT * FROM usuario WHERE user=:busca;");
                $rs->bindParam("busca",$busca);
                $rs->execute();
                if($rs != NULL){
                    foreach($rs as $item){
                        if($item['nome']!=""){
                            echo "<table border='1'>";
                            echo "<tr>";
                            echo "<th>ID</th>";
                            echo "<th>Nome</th>";
                            echo "<th>Usuário</th>";
                            echo "<th>E-Mail</th>";
                            echo "<th>Sexo</th>";
                            echo "<th>Senha</th>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td>{$item['id']}</td>";
                            echo "<td>{$item['nome']}</td>";
                            echo "<td>{$item['usr']}</td>";
                            echo "<td>{$item['email']}</td>";
                            echo "<td>{$item['sexo']}</td>";
                            echo "<td>{$item['senha']}</td>";
                            echo "</tr>";
                            echo "</table>";
                        }else{
                            echo "Usuário não encontrado";
                        }
                    }
                }
                $con->fecharConexao();
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function deletar(){                    // Comando DELETE
        
        }
        function inserir($usuario){                     // Comando INSERT
            try{
                $con=new Conexao();             // Abrir conexao
                $comando=$con->getConexao()->prepare("INSERT INTO usuario(nome,user,senha,email) VALUES (:n,:u,:s,:e);");
                $nome = $usuario->getNome();
                $user=$usuario->getUser();
                $email=$usuario->getEmail();
                $senha=$usuario->getSenha();
                $comando->bindParam("n",$nome);
                $comando->bindParam("u",$user);
                $comando->bindParam("e",$email);
                $comando->bindParam("s",$senha);
                if($comando->execute()){
                    $con->fecharConexao();
                    return true;
                }
                else{
                    $con->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function confirmSenha($senha,$conf){
            if ($senha != $conf) {
                return false;
            }else{
                return true;
            }
        }
        function atualizar(){               //Comando UPDATE
        
        }
        
    }

?>
