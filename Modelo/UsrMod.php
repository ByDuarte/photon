<?php
class Usuario{                  // Classe modelo Usuario
        // Atributos para encapsular
    private $nome;
    private $user;
    private $email;
    private $senha;
    private $senhaConf;
        // Encapsulamento dos atributos    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getNome(){
        return $this->nome;     // Retorna o valor de nome
    }
    public function setUser($user){
        $this->user=$user;
    }
    public function getUser(){
        return $this->user;     // Retorna o valor de user
    }
    public function setEmail($email){
        $this->email=$email;
    }
    public function getEmail(){
        return $this->email;    // Retorna o valor de email
    }
    public function setSenha($senha){
        $this->senha=$senha;
    }
    public function getSenha(){
        return $this->senha;    // Retorna o valor de senha
    }
    public function setConfSenha($senhaConf){
        $this->confSenha=$senhaConf;
    }
    public function getConfSenha(){
        return $this->confSenha;
    }
}
?>










