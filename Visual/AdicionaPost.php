<?php
session_start();
if(isset($_SESSION['nome'])){
	echo "<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <meta name='viewport' content='width=device-width, initial-scale=1'/>
  <title>Adicionar | PhotOn </title>

  <!-- CSS  -->
  <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
  <link href='materialize/css/materialize.css' type='text/css' rel='stylesheet' media='screen,projection'/>
  <link href='home.css' type='text/css' rel='stylesheet' media='screen,projection'/>
  <link href='Addpost.css' type='text/css' rel='stylesheet'/>
</head>
<body>
  <nav class='white' role='navigation'>
    <div class='nav-wrapper container'>
      <a id='logo-container' href='Home.php' class='brand-logo'><img src='Imagens/Logo.png' style='width: 55px; height: 55px;' id='logo'/></a>
    </div>
  </nav>

  <div id='index-banner' class='parallax-container'>
    <div class='section no-pad-bot'>
      <div class='container'>
        <br><br>
        <h1 class='header center teal-text text-lighten-2'>PhotOn</h1>
        <div class='row center'>
          <h5>Compartilhe sua experiência fotográfica!</h5>
        </div>
        <br><br>
      </div>
    </div>
    <div class='parallax'><img src='Imagens/banner.jpg' alt='banner' style='width: 100%;'></div>
  </div>


  <div class='container'>
    <div class='section'>
      <h5 align='center'>Adicione seu post</h5>
      <form action='../Controle/AddPostControl.php' method='POST' enctype='multipart/form-data'>
        <label for='titulo'>Insira o titulo do seu post: </label><input type='text' name='titulo' id='titulo' placeholder='Título'>
        <label for='textarea1'>Dê uma descrição sobre seu post: </label><textarea id='textarea1' class='materialize-textarea' name='descricao'></textarea>
        <h5>Selecione suas imagens: </h5>
        <input type='file' name='imagem'> <br>
        <input type='submit' name='posta' value='Postar' class='btnPost'>
    </div>
  </div>
  <footer class='page-footer teal'>
    <div class='container'>
      <div class='row'>
        <div class='col l6 s12'>
          <h5 class='white-text'>Sobre</h5>
          <p class='grey-text text-lighten-4'>Comunidade que tem por finalidade concentrar e compartilhar todo o mundo da fotografia, através da ajuda de pessoas dos mais diversos locais. Venha participar, aprenda e ensine!</p>


        </div>
        <div class='col l3 s12'>
          <h5 class='white-text'>Contatos</h5>
          <ul>
            <li><a class='white-text' href='#!'>Instagram</a></li>
            <li><a class='white-text' href='#!'>Facebook</a></li>
            <li><a class='white-text' href='#!'>Twitter</a></li>
          </ul>
        </div>

        <div class='col l3 s12'>
          <img align='center' src='Imagens/cameraFooter01.jpg' width='150' height='150'>
         
        </div>
  </footer>


  <!--  Scripts-->
  <script src='https://code.jquery.com/jquery-2.1.1.min.js'></script>
  <script src='js/materialize.js'></script>
  <script src='js/init.js'></script>

  </body>";
}else{
  header("Location: Login.php");
}
?>