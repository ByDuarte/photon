<?php
  session_start();
  if(isset($_SESSION['nome'])){
  require_once("../Controle/Estrucontrole.php");
  $control = new Postagem();
  $all = $control->selecionarPost();
	echo "<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <meta name='viewport' content='width=device-width, initial-scale=1'/>
  <title>Home | PhotOn </title>

  <!-- CSS  -->
  <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
  <link href='materialize/css/materialize.css' type='text/css' rel='stylesheet' media='screen,projection'/>
  <link href='home.css' type='text/css' rel='stylesheet' media='screen,projection'/>
</head>
<body>
  <nav class='white' role='navigation'>
    <div class='nav-wrapper container'>
      <a id='logo-container' href='Home.php' class='brand-logo'><img src='Imagens/Logo.png' style='width: 55px; height: 55px;' id='logo'/></a>
    </div>
  </nav>

  <div id='index-banner' class='parallax-container'>
    <div class='section no-pad-bot'>
      <div class='container'>
        <br><br>
        <h1 class='header center cyan-text'>PhotOn</h1>
        <div class='row center'>
          <h5 class='header center text-darken-2'>Compartilhe sua experiência fotográfica!</h5>
        </div>
        <br><br>
      </div>
    </div>
    <div class='parallax'><img src='Imagens/banner.png'></div>
  </div>


  <div class='container'>
    <div class='section'>

      <!--   Primeira seção   -->
      <div class='row'>
        <div class='col s6 m4'>
          <div class='icon-block'>
            <h5 class='center'>Conheça</h5>
            <p class='light'>Afim de criar um portifólio para organizar e divulgar seu trabalho? Você veio ao site certo. Aqui você terá o poder em suas mãos e poderá criar e publicar seus portifólios, além de conseguir visualizar os trabalhos de colegas da mesma área.</p>
          </div>
        </div>

        <div class='col s6 m4'>
          <div class='icon-block'>
            <h5 class='center'>Teste agora!</h5>
            <p class='light'>Cadastre-se e adicione seus posts!</p>
            <a href='AdicionaPost.php'><button id='gerencia' class='cyan-text'>Criar um post</button></a>
          </div>
        </div>

        <div class='col s12 m4'>
          <div class='icon-block'>
            <h5 class='center'>Bem Vindo!</h5>
            <img src='Imagens/cameraInicio.jpg' alt='Seja bem vindo!' style='width: 300px; height:300px' >
          </div>
        </div>
      </div>
      <h3 align='center'>Posts</h3>";
        if($all != NULL){
          foreach($all as $tudo){
            echo"<h5>{$tudo->getTitulo()}</h5>";
            echo"<p>{$tudo->getDesc()}</p>";
            echo"<a href='EditarPost.php'><input type='submit' value='Atualizar' class='btnPost'></a>";
            echo"<form action='../Controle/DeletaPost.php?titulo={$tudo->getTitulo()}' method='POST'><input type='submit' value='Deletar' class='btnPosts'></form>";
          }
        }
    echo"</div>
  </div>
  <footer class='page-footer teal'>
    <div class='container'>
      <div class='row'>
        <div class='col l6 s12'>
          <h5 class='white-text'>Sobre</h5>
          <p class='grey-text text-lighten-4'>Comunidade que tem por finalidade concentrar e compartilhar todo o mundo da fotografia, através da ajuda de pessoas dos mais diversos locais. Venha participar, aprenda e ensine!</p>


        </div>
        <div class='col l3 s12'>
          <h5 class='white-text'>Contatos</h5>
          <ul>
            <li><a class='white-text' href='#!'>Instagram</a></li>
            <li><a class='white-text' href='#!'>Facebook</a></li>
            <li><a class='white-text' href='#!'>Twitter</a></li>
          </ul>
        </div>

        <div class='col l3 s12'>
          <img align='center' src='Imagens/cameraFooter01.jpg' width='150' height='150'>
        </div>
  </footer>


  <!--  Scripts-->
  <script src='https://code.jquery.com/jquery-2.1.1.min.js'></script>
  <script src='js/materialize.js'></script>
  <script src='js/init.js'></script>

  </body>";
}else{
  header("Location: Login.php");
}
?>